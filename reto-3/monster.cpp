#include <iostream>
#include <random>

#include "cell.h"
#include "map.h"
#include "monster.h"

std::random_device device;
std::default_random_engine gen(device());
std::uniform_int_distribution<int> random_int(-1, 1);

void Monster::init(const Cell& cell) {
    position.row = cell.row;
    position.column = cell.column;
}

void Monster::move(const Map& map) {
    int delta_row = random_int(gen);
    int delta_column = random_int(gen);
    Cell jump = position.add(delta_row, delta_column);

    // Se comprueba que la nueva posición no sea una pared o sea la misma posición de antes
    while (map.is_wall(jump) || position == jump) {
        delta_row = random_int(gen);
        delta_column = random_int(gen);
        jump = position.add(delta_row, delta_column);
    }

    // se mueve a la nueva posición
    position.move(delta_row, delta_column);
}

const Cell Monster::get_position() const {
    return position;
}

bool Monster::is_position(const Cell& cell) const {
    return position == cell;
}