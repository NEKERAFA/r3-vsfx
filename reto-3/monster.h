#include "cell.h"
#include "game_state.h"

#ifndef MONSTER_H_
#define MONSTER_H_

class Monster {
    public:
        // Establece al monstruo en una posicion dada
        void init(const Cell& cell);
        // Mueve al monstruo a una posición aleatoria
        void move(const Map& map);
        // Obtiene la posición actual del monstruo
        const Cell get_position() const;

        // Imprime el mapa
        friend void print_map(Map&, Player&, Monster&);

    private:
        Cell position;

        // Comprueba si el monstruo está en la celda
        bool is_position(const Cell& cell) const;
};

#endif // MONSTER_H_