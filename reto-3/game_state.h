#ifndef GAME_STATE_H_
#define GAME_STATE_H_

// Representa el mapa del juego
class Map;

// Representa al jugador
class Player;

// Representa al monstruo
class Monster;

#endif // GAME_STATE_H_