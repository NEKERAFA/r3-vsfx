#include "vector2i.h"
#include "cell.h"

const Cell Cell::add(const int delta_row, const int delta_column) const {
    return { row + delta_row, column + delta_column };
}

void Cell::move(const int delta_row, const int delta_column) {
    row += delta_row;
    column += delta_column;
}

bool Cell::operator==(const Cell& other) const {
    return row == other.row && column == other.column;
}

bool Cell::operator!=(const Cell& other) const {
    return row != other.row || column != other.column;
}

const Vector2i Cell::to_vector() const {
    return { static_cast<int>(column), static_cast<int>(row) };
}