#include "vector2i.h"

#ifndef CELL_H_
#define CELL_H_

typedef unsigned int pos_t;

struct Cell {
    pos_t row = 0u;
    pos_t column = 0u;

    const Cell add(const int delta_row, const int delta_column) const;
    void move(const int delta_row, const int delta_column);

    bool operator==(const Cell& other) const;
    bool operator!=(const Cell& other) const;
    
    const Vector2i to_vector() const;
};

#endif // CELL_H_