#ifdef _WIN32
#include <SDL.h>
#else
#include <SDL2/SDL.h>
#endif

#ifndef CONFIG_H_
#define CONFIG_H_

// Título de la ventana
constexpr char WINDOW_TITLE[] = "PEC 3 - La cueva de los condenados";

// Tamaño de la ventana
const Uint32 SCREEN_WIDTH = 640;
const Uint32 SCREEN_HEIGHT = 480;

// Carpeta con los assets
constexpr char ASSETS[] = "assets";

#endif // CONFIG_H_