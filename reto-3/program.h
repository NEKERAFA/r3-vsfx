#ifdef _WIN32
#include <SDL.h>
#include <SDL_mixer.h>
#else
#include <SDL2/SDL.h>
#include <SDL2/SDL_mixer.h>
#endif

#include "cell.h"
#include "map.h"
#include "player.h"
#include "monster.h"

#ifndef PROGRAM_H_
#define PROGRAM_H_

class Program {
    public:
        Program() = default;
        ~Program();

        // Inicializa el programa
        bool init();
        // Se llama al invocar un evento de teclado
        void keyboard_event(SDL_KeyboardEvent& event);
        // Actualiza el programa
        void update();

    private:
        bool success;
        bool player_win;
        bool finished;

        Map map;
        Player player;
        Monster monster;

        Mix_Chunk* monster_sleeping;
        Mix_Chunk* monster_eating;
        Mix_Chunk* monster_walk;
        int monster_channel = -1;

        // Mueve aleatoriamente al monstruo y reproduce el sonido de pasos
        void move_monster();

        Mix_Chunk* player_walk;
        Mix_Chunk* player_wall;
        int player_channel = -1;

        /// @brief Actualiza la posición del jugador y reproduce el sonido según la celda que tenga delante
        /// @return true si el jugador se puede mover, false si se ha chocado con una pared
        bool move_player();

        Mix_Chunk* waterfall;
        int waterfall_channel = -1;

        Mix_Music* victory;
        // Pausa todos los sonidos y reproduce la música de victoria
        void play_victory();

        Mix_Music* gameover;
        // Pausa todos los sonidos y reproduce la música de gameover
        void play_gameover();

        // Coloca un sonido con respecto al jugador
        void listen_sound(int channel, Vector2i position, int range);
        // Actualiza la posición del audio del monstruo y del agua
        void update_sounds();
        // Pausa todos los sonidos
        void pause_sounds();
};

#endif // PROGRAM_H_