#include "cell.h"
#include "map.h"

bool Map::is_wall(const Cell& cell) const {
    return DATA[cell.row * ROWS + cell.column] == CellType::WALL;
}

bool Map::is_goal(const Cell& cell) const {
    return DATA[cell.row * ROWS + cell.column] == CellType::GOAL;
}

const Cell Map::get_goal() const {
    return { 8, 8 };
}
