#include "cell.h"
#include "direction.h"
#include "map.h"
#include "player.h"

void Player::init(const Cell& cell, const Direction::Value& look) {
    position.row = cell.row;
    position.column = cell.column;
    direction = look;
}

bool Player::move(const Map& map) {
    int delta_row = direction == Direction::DOWN ? 1 : (direction == Direction::UP ? -1 : 0);
    int delta_column = direction == Direction::RIGHT ? 1 : (direction == Direction::LEFT ? -1 : 0);

    if (map.is_wall(position.add(delta_row, delta_column))) {
        return false;
    }

    position.move(delta_row, delta_column);
    return true;
}

const Cell Player::get_position() const {
    return position;
}

Direction::Value Player::get_direction() const {
    return direction;
}

const Vector2i Player::get_look() const {
    return Direction::to_vector(direction);
}

void Player::rotate_clockwise() {
    switch (direction) {
        case Direction::UP:
            direction = Direction::RIGHT;
            break;
        case Direction::RIGHT:
            direction = Direction::DOWN;
            break;
        case Direction::DOWN:
            direction = Direction::LEFT;
            break;
        case Direction::LEFT:
            direction = Direction::UP;
            break;
    }
}

void Player::rotate_counterclockwise() {
    switch (direction) {
        case Direction::UP:
            direction = Direction::LEFT;
            break;
        case Direction::LEFT:
            direction = Direction::DOWN;
            break;
        case Direction::DOWN:
            direction = Direction::RIGHT;
            break;
        case Direction::RIGHT:
            direction = Direction::UP;
            break;
    }
}

bool Player::is_position(const Cell& cell) const {
    return position == cell;
}