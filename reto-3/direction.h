#include <string>
#include <ostream>

#include <stdint.h>

#include "vector2i.h"

#ifndef DIRECTION_H_
#define DIRECTION_H_

namespace Direction {
    enum Value : uint8_t {
        UP,
        RIGHT,
        DOWN,
        LEFT
    };

    const std::string to_string(Value value);
    std::ostream& operator<<(std::ostream& stream, Value value);

    const Vector2i to_vector(Value value);
};

#endif // DIRECTION_H_