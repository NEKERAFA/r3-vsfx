#include "cell.h"
#include "direction.h"
#include "game_state.h"

#ifndef PLAYER_H_
#define PLAYER_H_

class Player {
    public:
        // Establece al jugador jugador con la posición y dirección dada
        void init(const Cell& cell, const Direction::Value& look);
        /// @brief Mueve al jugador hacia donde esté mirando
        /// @return true si se puede mover hacia delante, false en otro caso
        bool move(const Map& map);
        // Obtiene la posición actual del jugador
        const Cell get_position() const;
        // Obtiene el vector a donde mira el jugador
        const Vector2i get_look() const;
        // Obtiene la dirección a donde mira el jugador
        Direction::Value get_direction() const;
        // Rota al jugador en sentido horario 90 grados
        void rotate_clockwise();
        // Rota al jugador en sentido antihorario 90 grados
        void rotate_counterclockwise();

        // Imprime el mapa
        friend void print_map(Map&, Player&, Monster&);
        // Imprime la direccion en la que mira el jugador
        friend void print_direction(Player&);
    private:
        Cell position;
        Direction::Value direction;

        // Comprueba si el jugador está en la celda
        bool is_position(const Cell& cell) const;
};

#endif // PLAYER_H_