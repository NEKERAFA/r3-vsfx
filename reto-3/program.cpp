#include <iostream>
#include <filesystem>

#ifdef _WIN32
#include <SDL.h>
#include <SDL_mixer.h>
#else
#include <SDL2/SDL.h>
#include <SDL2/SDL_mixer.h>
#endif

#include "config.h"
#include "vector2i.h"
#include "cell.h"
#include "direction.h"
#include "map.h"
#include "player.h"
#include "monster.h"
#include "program.h"

void print_map(Map& map, Player& player, Monster& monster) {
    for (pos_t row = 0; row < map.ROWS; row++) {
        for (pos_t column = 0; column < map.COLUMNS; column++) {
            switch (map.DATA[row * map.ROWS + column]) {
                case CellType::WALL:
                    std::cout << "█";
                    break;
                case CellType::GOAL:
                    std::cout << "░";
                    break;
                default:
                    Cell cell { row, column };
                    if (player.is_position(cell)) {
                        std::cout << "P";
                    } else if (monster.is_position(cell)) {
                        std::cout << "M";
                    } else {
                        std::cout << "·";
                    }
                    break;
            }
        }
        std::cout << std::endl;
    }
}

void print_direction(Player& player) {
    std::cout << player.direction << std::endl;
}

namespace fs = std::filesystem;

Program::~Program() {
    if (monster_sleeping != nullptr)
        Mix_FreeChunk(monster_sleeping);

    if (monster_eating != nullptr)
        Mix_FreeChunk(monster_eating);

    if (monster_walk != nullptr)
        Mix_FreeChunk(monster_walk);

    if (player_walk != nullptr)
        Mix_FreeChunk(player_walk);

    if (player_wall != nullptr)
        Mix_FreeChunk(player_wall);

    if (waterfall != nullptr)
        Mix_FreeChunk(waterfall);

    if (victory != nullptr)
        Mix_FreeMusic(victory);

    if (gameover != nullptr)
        Mix_FreeMusic(gameover);
}

fs::path ASSETS_FOLDER = fs::path(".") / ASSETS;

const Cell MONSTER_INIT = { 4u, 5u };
const Cell PLAYER_INIT = { 7u, 2u };
const Direction::Value PLAYER_LOOK = Direction::UP;

bool Program::init() {
    success = false;
    player_win = false;
    finished = false;

    player.init(PLAYER_INIT, PLAYER_LOOK);
    monster.init(MONSTER_INIT);

    if (monster_sleeping == nullptr) {
        monster_sleeping = Mix_LoadWAV((ASSETS_FOLDER / "monster_sleeping.wav").c_str());
        if (monster_sleeping == nullptr) {
            std::cerr << "Failed to load " << ASSETS_FOLDER / "monster_sleeping.wav" << "! SDL_mixer ERROR: " << Mix_GetError() << std::endl;
            return false;
        }
    }
    monster_channel = Mix_PlayChannel(monster_channel, monster_sleeping, -1);

    if (monster_eating == nullptr) {
        monster_eating = Mix_LoadWAV((ASSETS_FOLDER / "monster_eating.wav").c_str());
        if (monster_eating == nullptr) {
            std::cerr << "Failed to load " << ASSETS_FOLDER / "monster_eating.wav" << "! SDL_mixer ERROR: " << Mix_GetError() << std::endl;
            return false;
        }
        Mix_VolumeChunk(monster_eating, 96);
    }

    if (monster_walk == nullptr) {
        monster_walk = Mix_LoadWAV((ASSETS_FOLDER / "monster_walk.wav").c_str());
        if (monster_walk == nullptr) {
            std::cerr << "Failed to load " << ASSETS_FOLDER / "monster_walk.wav" << "! SDL_mixer ERROR: " << Mix_GetError() << std::endl;
            return false;
        }
    }

    if (player_walk == nullptr) {
        player_walk = Mix_LoadWAV((ASSETS_FOLDER / "player_walk.wav").c_str());
        if (player_walk == nullptr) {
            std::cerr << "Failed to load " << ASSETS_FOLDER / "player_walk.wav" << "! SDL_mixer ERROR: " << Mix_GetError() << std::endl;
            return false;
        }
        Mix_VolumeChunk(player_walk, 96);
    }

    if (player_wall == nullptr) {
        player_wall = Mix_LoadWAV((ASSETS_FOLDER / "player_wall.wav").c_str());
        if (player_wall == nullptr) {
            std::cerr << "Failed to load " << ASSETS_FOLDER / "player_wall.wav" << "! SDL_mixer ERROR: " << Mix_GetError() << std::endl;
            return false;
        }
        Mix_VolumeChunk(player_wall, 96);
    }

    if (waterfall == nullptr) {
        waterfall = Mix_LoadWAV((ASSETS_FOLDER / "water.wav").c_str());
        if (waterfall == nullptr) {
            std::cerr << "Failed to load " << ASSETS_FOLDER / "water.wav" << "! SDL_mixer ERROR: " << Mix_GetError() << std::endl;
            return false;
        }
    }
    waterfall_channel = Mix_PlayChannel(waterfall_channel, waterfall, -1);

    if (victory == nullptr) {
        victory = Mix_LoadMUS((ASSETS_FOLDER / "win.wav").c_str());
        if (victory == nullptr) {
            std::cerr << "Failed to load " << ASSETS_FOLDER / "win.wav" << "! SDL_mixer ERROR: " << Mix_GetError() << std::endl;
            return false;
        }
    }

    if (gameover == nullptr) {
        gameover = Mix_LoadMUS((ASSETS_FOLDER / "gameover.wav").c_str());
        if (gameover == nullptr) {
            std::cerr << "Failed to load " << ASSETS_FOLDER / "gameover.wav" << "! SDL_mixer ERROR: " << Mix_GetError() << std::endl;
            return false;
        }
    }

    Mix_VolumeMusic(128);

    update_sounds();
    print_map(map, player, monster);
    print_direction(player);
    return success = true;
}

void Program::keyboard_event(SDL_KeyboardEvent& event) {
    if (finished && !Mix_Playing(-1) && !Mix_PlayingMusic()) {
        init(); // Reinicia el juego
    } else if (!finished) {
        switch (event.keysym.sym) {
            case SDLK_UP:
            case SDLK_w:
                if (!move_player()) {
                    // Si el jugador no se puede mover, movemos al monstruo
                    move_monster();
                } else if (map.is_goal(player.get_position())) {
                    // Si el jugador llega al final, reproducimos la música de victoria
                    finished = true;
                    player_win = true;
                    play_victory();
                    std::cout << std::endl << "You win!" << std::endl;
                    return;
                }
                
                // Comprobamos si el monstruo nos ha comido
                if (monster.get_position() == player.get_position()) {
                    // Si nos ha comido, reproducimos el audio de comer
                    finished = true;
                    player_win = false;
                    Mix_HaltChannel(-1);
                    Mix_PlayChannel(monster_channel, monster_eating, 0);
                    std::cout << std::endl << "You lose!" << std::endl;
                    return;
                }

                // Actualizamos la posición de los sonidos
                update_sounds();

                std::cout << std::endl;
                print_map(map, player, monster);
                break;

            case SDLK_RIGHT:
            case SDLK_d:
                // Rotamos al jugador en sentido horario
                player.rotate_clockwise();
                // Se actualiza la posición de los sonidos
                update_sounds();

                std::cout << std::endl;
                print_direction(player);
                break;

            case SDLK_LEFT:
            case SDLK_a:
                // Rotamos al jugador en sentido antihorario
                player.rotate_counterclockwise();
                // Actualizamos la posición de los sonidos
                update_sounds();

                std::cout << std::endl;
                print_direction(player);
                break;
        }
    }
}

void Program::update() {
    if (!finished) {
        if (!Mix_Playing(monster_channel)) {
            // Si no se está repoduciendo ningún sonido del monstruo, ponemos el de dormir en modo loping
            Mix_PlayChannel(monster_channel, monster_sleeping, -1);
        }
    } else if (!player_win) {
        // Si se ha reproducido el sonido de comer pero no el de gameover, se reproduce
        if (!Mix_Playing(monster_channel) && !Mix_PlayingMusic()) {
            play_gameover();
            player_win = true; // HACK: pongo esto a true aunque no se haya ganado para que no se reproduzca de cada vez.
            // Se soluciona metiendo otra variable
        }
    }
}

void Program::listen_sound(int channel, Vector2i position, int range) {
    Vector2i player_pos = player.get_position().to_vector();
    Vector2i player_dir = player.get_look();
    Vector2i sound_dir = Vector2i(position.x - player_pos.x, player_pos.y - position.y); // Se hace flip de las y para calcular bien el ángulo

    // Se obtiene la distancia al sonido clampeada con el rango del sonido
    Uint8 distance = static_cast<Uint8>(255.0f * player_pos.distance(position) / range);
    // Se obtiene el ángulo de incidencia del sonido
    Sint16 angle = static_cast<Sint16>(sound_dir.angle_to(player_dir) * 180.0f / M_PI);
    if (angle < 0) angle += 360; // Para angulos negativos se invierte el giro para que pannee bien el sonido

    Mix_SetPosition(channel, angle, distance);
}

void Program::update_sounds() {
    listen_sound(monster_channel, monster.get_position().to_vector(), 3);
    listen_sound(waterfall_channel, map.get_goal().to_vector(), 12);
}

void Program::pause_sounds() {
    Mix_HaltChannel(-1);
}

void Program::move_monster() {
    monster.move(map);
    Mix_PlayChannel(monster_channel, monster_walk, 0);
}

bool Program::move_player() {
    bool move = player.move(map);
    if (move) {
        player_channel = Mix_PlayChannel(player_channel, player_walk, 0);
    } else {
        player_channel = Mix_PlayChannel(player_channel, player_wall, 0);
    }

    return move;
}

void Program::play_victory() {
    Mix_HaltChannel(-1);
    Mix_PlayMusic(victory, 0);
}

void Program::play_gameover() {
    Mix_HaltChannel(-1);
    Mix_PlayMusic(gameover, 0);
}