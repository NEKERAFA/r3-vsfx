#include <string>

#ifndef VECTOR_H_
#define VECTOR_H_

struct Vector2i {
    int x = 0;
    int y = 0;

    Vector2i() = default;
    Vector2i(int x, int y);

    // Unary positive
    Vector2i operator+() const;
    // Unary negative
    Vector2i operator-() const;
    // Add two vectors
    Vector2i operator+(const Vector2i& other) const;
    // Substract two vectors
    Vector2i operator-(const Vector2i& other) const;

    // Get the cross product between two vectors
    float cross(const Vector2i& other) const;
    // Get the dot product between two vectors
    float dot(const Vector2i& other) const;
    // Get the distance between two vectors
    float distance(const Vector2i& other) const;
    // Get the angle between two vectors
    float angle_to(const Vector2i& other) const;

    std::string to_string() const;
};

#endif // VECTOR_H_