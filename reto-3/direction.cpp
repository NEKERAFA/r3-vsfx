#include <string>
#include <ostream>

#include "vector2i.h"
#include "direction.h"

const std::string Direction::to_string(Direction::Value value) {
    switch (value) {
        case Direction::UP:
            return "Direction(↑)";
        case Direction::RIGHT:
            return "Direction(→)";
        case Direction::DOWN:
            return "Direction(↓)";
        case Direction::LEFT:
            return "Direction(←)";
    }

    return "";
}

std::ostream& Direction::operator<<(std::ostream& stream, Direction::Value value) {
    return stream << Direction::to_string(value);
}

const Vector2i Direction::to_vector(Direction::Value value) {
    switch (value) {
        case Value::UP:
            return Vector2i(0, 1);
        case Value::RIGHT:
            return Vector2i(1, 0);
        case Value::DOWN:
            return Vector2i(0, -1);
        case Value::LEFT:
            return Vector2i(-1, 0);
    }

    return Vector2i();
}