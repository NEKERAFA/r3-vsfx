#include "cell.h"
#include "game_state.h"

#ifndef MAP_H_
#define MAP_H_

enum CellType { 
    NONE,
    WALL,
    GOAL
};

class Map {
    public:
        const unsigned int ROWS = 10;
        const unsigned int COLUMNS = 10;

        // Comprueba si la celda es pared
        bool is_wall(const Cell& cell) const;
        // Comprueba si la celda es el salto del agua
        bool is_goal(const Cell& cell) const;
        // Obtiene la celda del salto del agua
        const Cell get_goal() const;

        // Imprime el mapa
        friend void print_map(Map&, Player&, Monster&);

    private:
        const CellType DATA[100] = {
            WALL, WALL, WALL, WALL, WALL, WALL, WALL, WALL, WALL, WALL,
            WALL, NONE, NONE, NONE, WALL, NONE, NONE, NONE, NONE, WALL,
            WALL, NONE, NONE, NONE, WALL, NONE, NONE, NONE, NONE, WALL,
            WALL, NONE, WALL, NONE, WALL, NONE, NONE, NONE, NONE, WALL,
            WALL, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, WALL,
            WALL, NONE, NONE, NONE, NONE, NONE, NONE, WALL, NONE, WALL,
            WALL, NONE, NONE, WALL, NONE, NONE, NONE, WALL, NONE, WALL,
            WALL, NONE, NONE, WALL, NONE, NONE, NONE, WALL, NONE, WALL,
            WALL, NONE, NONE, WALL, NONE, NONE, NONE, WALL, GOAL, WALL,
            WALL, WALL, WALL, WALL, WALL, WALL, WALL, WALL, WALL, WALL
        };
};

#endif // MAP_H_