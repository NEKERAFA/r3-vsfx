#include <string>
#include <cmath>

#include "vector2i.h"

Vector2i::Vector2i(int x, int y) : x(x), y(y) {}

Vector2i Vector2i::operator+() const {
    return Vector2i(+x, +y);
}

Vector2i Vector2i::operator-() const {
    return Vector2i(-x, -y);
}

Vector2i Vector2i::operator+(const Vector2i& other) const {
    return Vector2i(x + other.x, y + other.y);
}

Vector2i Vector2i::operator-(const Vector2i& other) const {
    return Vector2i(x - other.x, y - other.y);
}

float Vector2i::dot(const Vector2i& other) const {
    return x * other.x + y * other.y;
}

float Vector2i::cross(const Vector2i& other) const {
    return x * other.y - y * other.x;
}

float Vector2i::distance(const Vector2i& other) const {
    // return sqrt(pow(other.x - x, 2) + pow(other.y - y, 2));
    // best: https://en.cppreference.com/w/cpp/numeric/math/hypot
    return hypot(other.x - x, other.y - y);
}

float Vector2i::angle_to(const Vector2i& other) const {
    // https://stackoverflow.com/a/21486462 y https://github.com/godotengine/godot/blob/master/core/math/vector2.cpp#L81
    return atan2(cross(other), dot(other));
}

std::string Vector2i::to_string() const {
    return "Vector(" + std::to_string(x) + ", " + std::to_string(y) + ")";
}