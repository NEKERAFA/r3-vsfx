#include <iostream>

#ifdef _WIN32
#include <SDL.h>
#include <SDL_mixer.h>
#else
#include <SDL2/SDL.h>
#include <SDL2/SDL_mixer.h>
#endif

#include "config.h"
#include "program.h"

bool init_window(SDL_Window*& window) {
    // Se inicializa el módulo SDL
    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        std::cerr << "SDL could not initalize! SDL_Error: " << SDL_GetError() << std::endl;
        return false;
    }

    // Se inicializa el módulo SDL_MIX
    if (Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 2048) < 0) {
        std::cerr << "SDL_Mixer could not initialize! SDL_Mixer Error: " << Mix_GetError() << std::endl;
        return false;
    }

    // Se crea la ventana
    window = SDL_CreateWindow(WINDOW_TITLE, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_OPENGL | SDL_WINDOW_MOUSE_CAPTURE | SDL_WINDOW_SHOWN);
    if (window == NULL) {
        std::cerr << "Window could not be created! SDL_Error: " << SDL_GetError() << std::endl;
        return false;
    }

    return true;
}

void free_window(SDL_Window* window) {
    // Se destruye la ventana
    SDL_DestroyWindow(window);

    // Finalizamos los subsistemas de SDL
    Mix_Quit();
    SDL_Quit();
}

int main(int argc, const char* argv[]) {
    SDL_Window* window = NULL;

    // Se inicializa la ventana
    bool success = init_window(window);
    if (success) {
        // Se inicializa el programa
        Program* program = new Program();
        success = program->init();
        if (success) {
            // Inicializamos el bucle principal
            bool quit = false;
            SDL_Event event;

            while (!quit) {
                // Comprobamos los eventos
                while (SDL_PollEvent(&event)) {
                    switch(event.type) {
                        // Comprobamos si tenemos que finalizar el programa
                        case SDL_QUIT:
                            quit = true;
                            break;
                        // Comprobamos si se ha presionado la tecla ESCAPE
                        case SDL_KEYUP:
                            if (event.key.keysym.sym == SDLK_ESCAPE)
                                quit = true;
                            break;
                        // Se traslada el evento de la tecla al programa
                        case SDL_KEYDOWN:
                            program->keyboard_event(event.key);
                            break;
                    }

                    if (quit) break;
                }

                if (!quit) {
                    program->update();
                }
            }

            delete program;
        }

        free_window(window);
    }

    return success ? EXIT_SUCCESS : EXIT_FAILURE;
}