# Reto 3 - La Cueva de los Condenados

Repositorio de Rafael Alcalde Azpiazu para el Reto 3 de la asignatura de Efectos visuales y sonoros del Master Universitario en Diseño y Programación de Videojuegos de la UOC.

## ⚙️ Compilar el proyecto

No debería haber problema en las dependencias porque dentro de los archivos se incluye una comprobación para saber cómo incluir las cabeceras de SDL, que cambian entre Windows y Unix:

```cpp
#ifdef _WIN32
#include <SDL.h>
#include <SDL_mixer.h>
#else
#include <SDL2/SDL.h>
#include <SDL2/SDL_mixer.h>
#endif
```

## 🗃️ Estructura de archivos

El repositorio está dividido en carpetas por semanas, que contienen cada una los retos propuestos para cada semana. Se ha usado CMake para la compilación de cada reto.
